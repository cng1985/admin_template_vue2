import Vue from 'vue'
import App from './App.vue'
import router from './router';
import store from './store';

import ElementUI from 'element-ui';
import 'element-ui/lib/theme-chalk/index.css';
import tool from './utils/tool'
import config from "./config"
import http from "./utils/request"
import Vue2OrgTree from 'vue2-org-tree'
import 'font-awesome/css/font-awesome.min.css'
import 'vue2-org-tree/dist/style.css'

Vue.use(Vue2OrgTree)


Vue.prototype.$CONFIG = config;
Vue.prototype.$TOOL = tool;
Vue.prototype.$HTTP = http;


Vue.config.productionTip = false
Vue.use(ElementUI);




new Vue({
  el:'#app',
  router,
  store,
  render: h => h(App)
})
