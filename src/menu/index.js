export default [

    {
        name: "首页",
        path: "/home",
        icon:"",
        menuType:"menu",
        permission:"dish",
    },
    {
        name: "菜品中心",
        path: "/dish",
        permission:"dish",
        menuType:"menuDir",
        children:[
            {
                name: "菜品管理",
                path: "/dish/home",
                icon:"",
                menuType:"menu",
                permission:"dish",
            },
            {
                name: "菜品分组",
                path: "/dishgroup/home",
                icon:"",
                menuType:"menu",
                permission:"dishgroup",
            },
            {
                name: "套餐管理",
                path: "/dishsetmeal/home",
                icon:"",
                menuType:"menu",
                permission:"dishsetmeal",
            },
            {
                name: "属性管理",
                path: "/dishattr/home",
                icon:"",
                menuType:"menu",
                permission:"dishattr",
            },
        ]
    }


]