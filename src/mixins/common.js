export default {
    methods: {
        goBack() {
            this.$router.go(-1);
        },
        async getShopList() {
            var self = this;
            var param = {};
            param.sortMethod = "asc";
            param.sortField = "id";
            param.level = 1;
            param.size = 500;
            param.fetch = 0;
            var url = self.$CONFIG.REMOTE_URL + "/rest/shop/list.htm";
            var res = await this.$HTTP.form(url, param);
            if (res.code == 0) {
                self.shopOptions = res.list;
            }
        },
        postData(url, param, callback) {
            var self=this;
            this.$HTTP.form(url, param).then((res) => {
                if (res.code == 0) {
                    callback(res);
                } else {
                    self.$message.error(res.msg);
                }
            });
        },
        postJsonData(url, param, callback) {
            var self=this;
            this.$HTTP.post(url, param).then((res) => {
                if (res.code == 0) {
                    callback(res);
                } else {
                    self.$message.error(res.msg);
                }
            });
        }
    },
}