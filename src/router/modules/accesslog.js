export default [
    {
        name: "home",
        path: "/accesslog/index",
        component: () => import(`@/views/accesslog/index`),
        meta: {
            title: "访问日志管理",
            icon: "el-icon-platform-eleme"
        }
    },
    {
        name: "home",
        path: "/accesslog/add",
        component: () => import(`@/views/accesslog/add`),
        meta: {
            title: "添加访问日志",
            icon: "el-icon-platform-eleme"
        }
    },
    {
        name: "home",
        path: "/accesslog/update",
        component: () => import(`@/views/accesslog/update`),
        meta: {
            title: "更新访问日志",
            icon: "el-icon-platform-eleme"
        }
    },
    {
        name: "home",
        path: "/accesslog/view",
        component: () => import(`@/views/accesslog/view`),
        meta: {
            title: "访问日志详情",
            icon: "el-icon-platform-eleme"
        }
    }
]