export default [
    {
        name: "ad_home",
        path: "/ad/index",
        component: () => import(`@/views/ad/index`),
        meta: {
            title: "管理",
            icon: "el-icon-platform-eleme"
        }
    },
    {
        name: "ad_add",
        path: "/ad/add",
        component: () => import(`@/views/ad/add`),
        meta: {
            title: "添加",
            icon: "el-icon-platform-eleme"
        }
    },
    {
        name: "ad_update",
        path: "/ad/update",
        component: () => import(`@/views/ad/update`),
        meta: {
            title: "更新",
            icon: "el-icon-platform-eleme"
        }
    },
    {
        name: "ad_view",
        path: "/ad/view",
        component: () => import(`@/views/ad/view`),
        meta: {
            title: "详情",
            icon: "el-icon-platform-eleme"
        }
    },
]