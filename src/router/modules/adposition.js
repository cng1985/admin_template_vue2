export default [
    {
        name: "adposition_home",
        path: "/adposition/index",
        component: () => import(`@/views/adposition/index`),
        meta: {
            title: "管理",
            icon: "el-icon-platform-eleme"
        }
    },
    {
        name: "adposition_add",
        path: "/adposition/add",
        component: () => import(`@/views/adposition/add`),
        meta: {
            title: "添加",
            icon: "el-icon-platform-eleme"
        }
    },
    {
        name: "adposition_update",
        path: "/adposition/update",
        component: () => import(`@/views/adposition/update`),
        meta: {
            title: "更新",
            icon: "el-icon-platform-eleme"
        }
    },
    {
        name: "adposition_view",
        path: "/adposition/view",
        component: () => import(`@/views/adposition/view`),
        meta: {
            title: "详情",
            icon: "el-icon-platform-eleme"
        }
    },
]