export default [
    {
        name: "article_home",
        path: "/article/index",
        component: () => import(`@/views/article/index`),
        meta: {
            title: "文章管理",
            icon: "el-icon-platform-eleme"
        }
    },
    {
        name: "article_add",
        path: "/article/add",
        component: () => import(`@/views/article/add`),
        meta: {
            title: "添加文章",
            icon: "el-icon-platform-eleme"
        }
    },
    {
        name: "article_update",
        path: "/article/update",
        component: () => import(`@/views/article/update`),
        meta: {
            title: "更新文章",
            icon: "el-icon-platform-eleme"
        }
    },
    {
        name: "article_layout",
        path: "/article/view_layout",
        component: () => import(`@/views/article/view_layout`),
        meta: {
            title: "文章详情",
            icon: "el-icon-platform-eleme"
        },
        children:[
            {
                name: "article_view",
                path: "/article/view",
                component: () => import(`@/views/article/view`),
                meta: {
                    title: "文章详情",
                    icon: "el-icon-platform-eleme"
                }
            }
        ]

    }
]