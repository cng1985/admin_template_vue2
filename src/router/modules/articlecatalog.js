export default [
    {
        name: "articlecatalog_home",
        path: "/articlecatalog/index",
        component: () => import(`@/views/articlecatalog/index`),
        meta: {
            title: "文章分类管理",
            icon: "el-icon-platform-eleme"
        }
    }
]