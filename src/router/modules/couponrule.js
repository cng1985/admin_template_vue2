export default [
    {
        name: "couponrule_home",
        path: "/couponrule/index",
        component: () => import(`@/views/couponrule/index`),
        meta: {
            title: "优惠券管理",
            icon: "el-icon-platform-eleme"
        }
    },
    {
        name: "couponrule_add",
        path: "/couponrule/add",
        component: () => import(`@/views/couponrule/add`),
        meta: {
            title: "添加优惠券",
            icon: "el-icon-platform-eleme"
        }
    },
    {
        name: "couponrule_update",
        path: "/couponrule/update",
        component: () => import(`@/views/couponrule/update`),
        meta: {
            title: "更新优惠券",
            icon: "el-icon-platform-eleme"
        }
    },
    {
        name: "couponrule_view",
        path: "/couponrule/view",
        component: () => import(`@/views/couponrule/view`),
        meta: {
            title: "优惠券详情",
            icon: "el-icon-platform-eleme"
        }
    },
]