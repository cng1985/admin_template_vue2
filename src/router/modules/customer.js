export default [{
        name: "home",
        path: "/customer/index",
        component: () => import(`@/views/customer/index`),
        meta: {
            title: "用户管理",
            icon: "el-icon-platform-eleme"
        }
    },
    {
        name: "home",
        path: "/customer/add",
        component: () => import(`@/views/customer/add`),
        meta: {
            title: "添加用户",
            icon: "el-icon-platform-eleme"
        }
    },
    {
        name: "home",
        path: "/customer/update",
        component: () => import(`@/views/customer/update`),
        meta: {
            title: "更新用户",
            icon: "el-icon-platform-eleme"
        }
    },
    {
        name: "home",
        path: "/customer/view_layout",
        component: () => import(`@/views/customer/view_layout`),
        meta: {
            title: "用户详情",
            icon: "el-icon-platform-eleme"
        },
        children: [{
                name: "home",
                path: "/customer/view",
                component: () => import(`@/views/customer/view`),
                meta: {
                    title: "更新用户",
                    icon: "el-icon-platform-eleme"
                }
            },
            {
                name: "home",
                path: "/customer/score",
                component: () => import(`@/views/customer/score`),
                meta: {
                    title: "更新用户",
                    icon: "el-icon-platform-eleme"
                }
            },
            {
                name: "home",
                path: "/customer/stream",
                component: () => import(`@/views/customer/stream`),
                meta: {
                    title: "更新用户",
                    icon: "el-icon-platform-eleme"
                }
            },
            {
                name: "home",
                path: "/customer/address",
                component: () => import(`@/views/customer/address`),
                meta: {
                    title: "更新用户",
                    icon: "el-icon-platform-eleme"
                }
            }
        ]
    }
]