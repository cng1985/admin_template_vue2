export default {

    name: "home",
    path: "/dish/index",
    permission:"dish",
    meneType:"menu",
    component: () => import(`@/views/dish/layout`),
    meta: {
        title: "菜品管理",
        icon: "el-icon-platform-eleme"
    },
    children: [{
        name: "home",
        path: "/dish/home",
        permission:"dish",
        meneType:"menu",
        component: () => import(`@/views/dish/home`),
        meta: {
            title: "菜品管理",
            icon: "el-icon-platform-eleme"
        }
    }, {
        name: "home",
        path: "/dish/add",
        component: () => import(`@/views/dish/dish_add`),
        meta: {
            title: "添加菜品",
            icon: "el-icon-platform-eleme"
        }
    }, {
        name: "room",
        path: "/dish/room",
        component: () => import(`@/views/dish/room`),
        meta: {
            title: "菜品管理",
            icon: "el-icon-platform-eleme"
        }
    }]
}