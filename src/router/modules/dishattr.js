export default [
    {
        name: "home",
        path: "/dishattr/index",
        component: () => import(`@/views/dishattr/index`),
        meta: {
            title: "属性管理",
            icon: "el-icon-platform-eleme"
        }
    },
    {
        name: "home",
        path: "/dishattr/add",
        component: () => import(`@/views/dishattr/add`),
        meta: {
            title: "添加属性",
            icon: "el-icon-platform-eleme"
        }
    },
    {
        name: "home",
        path: "/dishattr/update",
        component: () => import(`@/views/dishattr/update`),
        meta: {
            title: "更新属性",
            icon: "el-icon-platform-eleme"
        }
    },
    {
        name: "home",
        path: "/dishattr/view",
        component: () => import(`@/views/dishattr/view`),
        meta: {
            title: "属性详情",
            icon: "el-icon-platform-eleme"
        }
    }
]