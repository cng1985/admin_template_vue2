export default [
    {
        name: "home",
        path: "/dishgroup/index",
        component: () => import(`@/views/dishgroup/index`),
        meta: {
            title: "菜品分组管理",
            icon: "el-icon-platform-eleme"
        }
    },
    {
        name: "home",
        path: "/dishgroup/add",
        component: () => import(`@/views/dishgroup/add`),
        meta: {
            title: "添加菜品分组",
            icon: "el-icon-platform-eleme"
        }
    },
    {
        name: "home",
        path: "/dishgroup/update",
        component: () => import(`@/views/dishgroup/update`),
        meta: {
            title: "更新菜品分组",
            icon: "el-icon-platform-eleme"
        }
    },
    {
        name: "home",
        path: "/dishgroup/view",
        component: () => import(`@/views/dishgroup/view`),
        meta: {
            title: "菜品分组详情",
            icon: "el-icon-platform-eleme"
        }
    }
]