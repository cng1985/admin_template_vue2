export default [{

        name: "home",
        path: "/dishorder/index",
        component: () => import(`@/views/dishorder/index`),
        meta: {
            title: "订单管理",
            icon: "el-icon-platform-eleme"
        }
    },
    {

        name: "home",
        path: "/dishorder/view",
        component: () => import(`@/views/dishorder/view`),
        meta: {
            title: "订单详情",
            icon: "el-icon-platform-eleme"
        }
    }
]