export default [
    {
        name: "home",
        path: "/dishsetmeal/index",
        component: () => import(`@/views/dishsetmeal/index`),
        meta: {
            title: "套餐管理",
            icon: "el-icon-platform-eleme"
        }
    },
    {
        name: "home",
        path: "/dishsetmeal/add",
        component: () => import(`@/views/dishsetmeal/add`),
        meta: {
            title: "添加套餐",
            icon: "el-icon-platform-eleme"
        }
    },
    {
        name: "home",
        path: "/dishsetmeal/update",
        component: () => import(`@/views/dishsetmeal/update`),
        meta: {
            title: "更新套餐",
            icon: "el-icon-platform-eleme"
        }
    },
    {
        name: "home",
        path: "/dishsetmeal/view",
        component: () => import(`@/views/dishsetmeal/view`),
        meta: {
            title: "套餐详情",
            icon: "el-icon-platform-eleme"
        }
    }
]