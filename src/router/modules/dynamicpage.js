export default [
    {
        name: "dynamicpage_home",
        path: "/dynamicpage/index",
        component: () => import(`@/views/dynamicpage/index`),
        meta: {
            title: "动态页面管理",
            icon: "el-icon-platform-eleme"
        }
    },
    {
        name: "dynamicpage_add",
        path: "/dynamicpage/add",
        component: () => import(`@/views/dynamicpage/add`),
        meta: {
            title: "添加动态页面",
            icon: "el-icon-platform-eleme"
        }
    },
    {
        name: "dynamicpage_update",
        path: "/dynamicpage/update",
        component: () => import(`@/views/dynamicpage/update`),
        meta: {
            title: "更新动态页面",
            icon: "el-icon-platform-eleme"
        }
    },
    {
        name: "dynamicpage_view",
        path: "/dynamicpage/view",
        component: () => import(`@/views/dynamicpage/view`),
        meta: {
            title: "动态页面详情",
            icon: "el-icon-platform-eleme"
        }
    },
]