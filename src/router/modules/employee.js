export default [
    {
        name: "employee_home",
        path: "/employee/index",
        component: () => import(`@/views/employee/index`),
        meta: {
            title: "管理",
            icon: "el-icon-platform-eleme"
        }
    },
    {
        name: "employee_add",
        path: "/employee/add",
        component: () => import(`@/views/employee/add`),
        meta: {
            title: "添加",
            icon: "el-icon-platform-eleme"
        }
    },
    {
        name: "employee_update",
        path: "/employee/update",
        component: () => import(`@/views/employee/update`),
        meta: {
            title: "更新",
            icon: "el-icon-platform-eleme"
        }
    },
    {
        name: "employee_view",
        path: "/employee/view",
        component: () => import(`@/views/employee/view`),
        meta: {
            title: "详情",
            icon: "el-icon-platform-eleme"
        }
    },
]