export default [
    {
        name: "fullreduction_home",
        path: "/fullreduction/index",
        component: () => import(`@/views/fullreduction/index`),
        meta: {
            title: "满减活动管理",
            icon: "el-icon-platform-eleme"
        }
    },
    {
        name: "fullreduction_add",
        path: "/fullreduction/add",
        component: () => import(`@/views/fullreduction/add`),
        meta: {
            title: "添加满减活动",
            icon: "el-icon-platform-eleme"
        }
    },
    {
        name: "fullreduction_update",
        path: "/fullreduction/update",
        component: () => import(`@/views/fullreduction/update`),
        meta: {
            title: "更新满减活动",
            icon: "el-icon-platform-eleme"
        }
    },
    {
        name: "fullreduction_view",
        path: "/fullreduction/view",
        component: () => import(`@/views/fullreduction/view`),
        meta: {
            title: "满减活动详情",
            icon: "el-icon-platform-eleme"
        }
    },
]