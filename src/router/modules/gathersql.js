export default [
    {
        name: "gathersql_home",
        path: "/gathersql/index",
        component: () => import(`@/views/gathersql/index`),
        meta: {
            title: "聚集sql管理",
            icon: "el-icon-platform-eleme"
        }
    },
    {
        name: "gathersql_add",
        path: "/gathersql/add",
        component: () => import(`@/views/gathersql/add`),
        meta: {
            title: "添加聚集sql",
            icon: "el-icon-platform-eleme"
        }
    },
    {
        name: "gathersql_update",
        path: "/gathersql/update",
        component: () => import(`@/views/gathersql/update`),
        meta: {
            title: "更新聚集sql",
            icon: "el-icon-platform-eleme"
        }
    },
    {
        name: "gathersql_view",
        path: "/gathersql/view",
        component: () => import(`@/views/gathersql/view`),
        meta: {
            title: "聚集sql详情",
            icon: "el-icon-platform-eleme"
        }
    },
]