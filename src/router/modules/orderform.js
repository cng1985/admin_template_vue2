export default [
    {
        name: "orderform_home",
        path: "/orderform/index",
        component: () => import(`@/views/orderform/index`),
        meta: {
            title: "订单管理",
            icon: "el-icon-platform-eleme"
        }
    },
    {
        name: "orderform_add",
        path: "/orderform/add",
        component: () => import(`@/views/orderform/add`),
        meta: {
            title: "添加订单",
            icon: "el-icon-platform-eleme"
        }
    },
    {
        name: "orderform_update",
        path: "/orderform/update",
        component: () => import(`@/views/orderform/update`),
        meta: {
            title: "更新订单",
            icon: "el-icon-platform-eleme"
        }
    },
    {
        name: "orderform_view",
        path: "/orderform/view",
        component: () => import(`@/views/orderform/view`),
        meta: {
            title: "订单详情",
            icon: "el-icon-platform-eleme"
        }
    },
]