export default [
    {
        name: "home",
        path: "/organization/index",
        component: () => import(`@/views/organization/index`),
        meta: {
            title: "组织架构管理",
            icon: "el-icon-platform-eleme"
        }
    }
]