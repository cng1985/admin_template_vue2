export default [
    {
        name: "payconfig_home",
        path: "/payconfig/index",
        component: () => import(`@/views/payconfig/index`),
        meta: {
            title: "支付配置管理",
            icon: "el-icon-platform-eleme"
        }
    },
    {
        name: "payconfig_add",
        path: "/payconfig/add",
        component: () => import(`@/views/payconfig/add`),
        meta: {
            title: "添加支付配置",
            icon: "el-icon-platform-eleme"
        }
    },
    {
        name: "payconfig_update",
        path: "/payconfig/update",
        component: () => import(`@/views/payconfig/update`),
        meta: {
            title: "更新支付配置",
            icon: "el-icon-platform-eleme"
        }
    },
    {
        name: "payconfig_layout",
        path: "/payconfig/view_layout",
        component: () => import(`@/views/payconfig/view_layout`),
        meta: {
            title: "支付配置详情",
            icon: "el-icon-platform-eleme"
        },
        children:[
            {
                name: "payconfig_view",
                path: "/payconfig/view",
                component: () => import(`@/views/payconfig/view`),
                meta: {
                    title: "支付配置详情",
                    icon: "el-icon-platform-eleme"
                }
            }
        ]

    }
]