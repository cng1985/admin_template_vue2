export default [
    {
        name: "productgroup_home",
        path: "/productgroup/index",
        component: () => import(`@/views/productgroup/index`),
        meta: {
            title: "产品分组管理",
            icon: "el-icon-platform-eleme"
        }
    },
    {
        name: "productgroup_add",
        path: "/productgroup/add",
        component: () => import(`@/views/productgroup/add`),
        meta: {
            title: "添加产品分组",
            icon: "el-icon-platform-eleme"
        }
    },
    {
        name: "productgroup_update",
        path: "/productgroup/update",
        component: () => import(`@/views/productgroup/update`),
        meta: {
            title: "更新产品分组",
            icon: "el-icon-platform-eleme"
        }
    },
    {
        name: "productgroup_view",
        path: "/productgroup/view",
        component: () => import(`@/views/productgroup/view`),
        meta: {
            title: "产品分组详情",
            icon: "el-icon-platform-eleme"
        }
    },
]