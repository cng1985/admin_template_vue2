export default [
    {
        name: "home",
        path: "/shop/index",
        component: () => import(`@/views/shop/index`),
        meta: {
            title: "商家管理",
            icon: "el-icon-platform-eleme"
        }
    },
    {
        name: "home",
        path: "/shop/add",
        component: () => import(`@/views/shop/add`),
        meta: {
            title: "添加商家",
            icon: "el-icon-platform-eleme"
        }
    },
    {
        name: "home",
        path: "/shop/update",
        component: () => import(`@/views/shop/update`),
        meta: {
            title: "更新商家",
            icon: "el-icon-platform-eleme"
        }
    },
    {
        name: "home",
        path: "/shop/view_layout",
        component: () => import(`@/views/shop/view_layout`),
        meta: {
            title: "商家详情",
            icon: "el-icon-platform-eleme"
        },
        children:[
            {
                name: "home",
                path: "/shop/view",
                component: () => import(`@/views/shop/view`),
                meta: {
                    title: "商家详情",
                    icon: "el-icon-platform-eleme"
                }
            },
            {
                name: "home",
                path: "/shop/qrcode",
                component: () => import(`@/views/shop/qrcode`),
                meta: {
                    title: "商家二维码",
                    icon: "el-icon-platform-eleme"
                }
            }
        ]

    }
]