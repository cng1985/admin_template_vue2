export default [
    {
        name: "shopcash_home",
        path: "/shopcash/index",
        component: () => import(`@/views/shopcash/index`),
        meta: {
            title: "提现记录管理",
            icon: "el-icon-platform-eleme"
        }
    },
    {
        name: "shopcash_add",
        path: "/shopcash/add",
        component: () => import(`@/views/shopcash/add`),
        meta: {
            title: "添加提现记录",
            icon: "el-icon-platform-eleme"
        }
    },
    {
        name: "shopcash_update",
        path: "/shopcash/update",
        component: () => import(`@/views/shopcash/update`),
        meta: {
            title: "更新提现记录",
            icon: "el-icon-platform-eleme"
        }
    },
    {
        name: "shopcash_layout",
        path: "/shopcash/view_layout",
        component: () => import(`@/views/shopcash/view_layout`),
        meta: {
            title: "提现记录详情",
            icon: "el-icon-platform-eleme"
        },
        children:[
            {
                name: "shopcash_view",
                path: "/shopcash/view",
                component: () => import(`@/views/shopcash/view`),
                meta: {
                    title: "提现记录详情",
                    icon: "el-icon-platform-eleme"
                }
            }
        ]

    }
]