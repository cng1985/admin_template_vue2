export default [
    {
        name: "shopcatalog_home",
        path: "/shopcatalog/index",
        component: () => import(`@/views/shopcatalog/index`),
        meta: {
            title: "商家分类管理",
            icon: "el-icon-platform-eleme"
        }
    }
]