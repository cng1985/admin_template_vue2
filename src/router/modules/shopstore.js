export default [
    {
        name: "shopstore_home",
        path: "/shopstore/index",
        component: () => import(`@/views/shopstore/index`),
        meta: {
            title: "门店管理",
            icon: "el-icon-platform-eleme"
        }
    },
    {
        name: "shopstore_add",
        path: "/shopstore/add",
        component: () => import(`@/views/shopstore/add`),
        meta: {
            title: "添加门店",
            icon: "el-icon-platform-eleme"
        }
    },
    {
        name: "shopstore_update",
        path: "/shopstore/update",
        component: () => import(`@/views/shopstore/update`),
        meta: {
            title: "更新门店",
            icon: "el-icon-platform-eleme"
        }
    },
    {
        name: "shopstore_layout",
        path: "/shopstore/view_layout",
        component: () => import(`@/views/shopstore/view_layout`),
        meta: {
            title: "门店详情",
            icon: "el-icon-platform-eleme"
        },
        children:[
            {
                name: "shopstore_view",
                path: "/shopstore/view",
                component: () => import(`@/views/shopstore/view`),
                meta: {
                    title: "门店详情",
                    icon: "el-icon-platform-eleme"
                }
            }
        ]

    }
]