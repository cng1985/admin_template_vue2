export default [
    {
        name: "home",
        path: "/staff/index",
        component: () => import(`@/views/staff/index`),
        meta: {
            title: "员工管理",
            icon: "el-icon-platform-eleme"
        }
    },
    {
        name: "home",
        path: "/staff/add",
        component: () => import(`@/views/staff/add`),
        meta: {
            title: "添加员工",
            icon: "el-icon-platform-eleme"
        }
    },
    {
        name: "home",
        path: "/staff/update",
        component: () => import(`@/views/staff/update`),
        meta: {
            title: "更新员工",
            icon: "el-icon-platform-eleme"
        }
    },
    {
        name: "home",
        path: "/staff/view_layout",
        component: () => import(`@/views/staff/view_layout`),
        meta: {
            title: "员工详情",
            icon: "el-icon-platform-eleme"
        },
        children:[
            {
                name: "home",
                path: "/staff/view",
                component: () => import(`@/views/staff/view`),
                meta: {
                    title: "员工详情",
                    icon: "el-icon-platform-eleme"
                }
            }
        ]

    }
]