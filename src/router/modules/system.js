export default {

    name: "home",
    path: "/system/layour",
    component: () => import(`@/views/system/layout`),
    meta: {
        title: "菜品管理",
        icon: "el-icon-platform-eleme"
    },
    children: [{
        name: "home",
        path: "/system/index",
        component: () => import(`@/views/system/index`),
        meta: {
            title: "系统信息",
            icon: "el-icon-platform-eleme"
        }
    }, {
        name: "home",
        path: "/system/setting",
        component: () => import(`@/views/system/setting`),
        meta: {
            title: "系统设置",
            icon: "el-icon-platform-eleme"
        }
    }]
}