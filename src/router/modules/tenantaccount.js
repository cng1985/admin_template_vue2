export default [{
        name: "tenantaccount_home",
        path: "/tenantaccount/index",
        component: () => import(`@/views/tenantaccount/index`),
        meta: {
            title: "资金账户管理",
            icon: "el-icon-platform-eleme"
        }
    },
    {
        name: "tenantaccount_add",
        path: "/tenantaccount/add",
        component: () => import(`@/views/tenantaccount/add`),
        meta: {
            title: "添加资金账户",
            icon: "el-icon-platform-eleme"
        }
    },
    {
        name: "tenantaccount_update",
        path: "/tenantaccount/update",
        component: () => import(`@/views/tenantaccount/update`),
        meta: {
            title: "更新资金账户",
            icon: "el-icon-platform-eleme"
        }
    },
    {
        name: "tenantaccount_layout",
        path: "/tenantaccount/view_layout",
        component: () => import(`@/views/tenantaccount/view_layout`),
        meta: {
            title: "资金账户详情",
            icon: "el-icon-platform-eleme"
        },
        children: [{
            name: "tenantaccount_view",
            path: "/tenantaccount/view",
            component: () => import(`@/views/tenantaccount/view`),
            meta: {
                title: "资金账户详情",
                icon: "el-icon-platform-eleme"
            }
        }, {
            name: "tenantaccount_stream",
            path: "/tenantaccount/stream",
            component: () => import(`@/views/tenantaccount/stream`),
            meta: {
                title: "资金流水",
                icon: "el-icon-platform-eleme"
            }
        }]

    }
]