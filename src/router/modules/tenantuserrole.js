export default [
    {
        name: "home",
        path: "/tenantuserrole/index",
        component: () => import(`@/views/tenantuserrole/index`),
        meta: {
            title: "角色管理",
            icon: "el-icon-platform-eleme"
        }
    },
    {
        name: "home",
        path: "/tenantuserrole/add",
        component: () => import(`@/views/tenantuserrole/add`),
        meta: {
            title: "添加角色",
            icon: "el-icon-platform-eleme"
        }
    },
    {
        name: "home",
        path: "/tenantuserrole/update",
        component: () => import(`@/views/tenantuserrole/update`),
        meta: {
            title: "更新角色",
            icon: "el-icon-platform-eleme"
        }
    },
    {
        name: "home",
        path: "/tenantuserrole/view",
        component: () => import(`@/views/tenantuserrole/view`),
        meta: {
            title: "角色详情",
            icon: "el-icon-platform-eleme"
        }
    },
]