export default {

    name: "home",
    path: "/usercenter/index",
    component: () => import(`@/views/usercenter/index`),
    meta: {
        title: "个人信息",
        icon: "el-icon-platform-eleme"
    }
}