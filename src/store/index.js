import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

const files = require.context('./modules', false, /\.js$/);
const states = {};
const mutations = {};

files.keys().forEach((key) => {
  var items = files(key).default
  for (const key in items.state) {
    if (Object.hasOwnProperty.call(items.state, key)) {
      const element = items.state[key];
      states[key]=element;
    }
  }
  for (const key in items.mutations) {
    if (Object.hasOwnProperty.call(items.mutations, key)) {
      const element = items.mutations[key];
      mutations[key]=element;
    }
  }
})




const store = new Vuex.Store({
  state:states,
  mutations: mutations
})
var mo=store.state.ismobile;
console.info("+++"+mo);

console.info(store.state.ismobile);



export default store;