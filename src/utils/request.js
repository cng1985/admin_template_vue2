import axios from 'axios';
import {
	Notification,
	MessageBox
} from 'element-ui';
import sysConfig from "@/config";
import tool from '@/utils/tool';
import router from '@/router';
import qs from 'qs'

axios.defaults.baseURL = ''

axios.defaults.timeout = sysConfig.TIMEOUT
//axios.defaults.withCredentials = true


// HTTP request 拦截器
axios.interceptors.request.use(
	(config) => {
		let sessionToken = tool.data.get("sessionToken");
		if (sessionToken) {
			config.headers["sessionToken"] = sessionToken;
		}
		let sessionId = tool.data.get("sessionId");

		if (sessionId) {
			config.headers["sessionId"] = sessionId;
		}
		config.headers["tenant"] = sysConfig.TENANT;
		config.headers["client"] = "newbyte";
		if (!sysConfig.REQUEST_CACHE && config.method == 'get') {
			config.params = config.params || {};
			config.params['_'] = new Date().getTime();
		}
		Object.assign(config.headers, sysConfig.HEADERS)
		return config;
	},
	(error) => {
		return Promise.reject(error);
	}
);

// HTTP response 拦截器
axios.interceptors.response.use(
	(response) => {
		return response;
	},
	(error) => {
		if (error.response) {
			if (error.response.status == 404) {
				Notification.error({
					title: '请求错误',
					message: "Status:404，正在请求不存在的服务器记录！"
				});
			} else if (error.response.status == 500) {
				Notification.error({
					title: '请求错误',
					message: error.response.data.message || "Status:500，服务器发生错误！"
				});
			} else if (error.response.status == 401) {
				MessageBox.confirm('当前用户已被登出或无权限访问当前资源，请尝试重新登录后再操作。', '无权限访问', {
					type: 'error',
					closeOnClickModal: false,
					center: true,
					confirmButtonText: '重新登录'
				}).then(() => {
					router.replace({
						path: '/login'
					});
				}).catch(() => {})
			} else if (error.response.status == 302) {
				MessageBox.confirm('当前用户已被登出或无权限访问当前资源，请尝试重新登录后再操作。', '无权限访问', {
					type: 'error',
					closeOnClickModal: false,
					center: true,
					confirmButtonText: '重新登录'
				}).then(() => {
					router.replace({
						path: '/login'
					});
				}).catch(() => {})
			} else {
				Notification.error({
					title: '请求错误',
					message: error.response.data.message || `Status:${error.response.status}，未知错误！`
				});
			}
		} else {
			Notification.error({
				title: '请求错误',
				message: "请求服务器无响应！"
			});
		}

		return Promise.reject(error.response);
	}
);

var http = {

	/** get 请求
	 * @param  {接口地址} url
	 * @param  {请求参数} params
	 * @param  {参数} config
	 */
	get: function (url, params = {}, config = {}) {
		if (url.indexOf("http") == -1) {
			url = sysConfig.REMOTE_URL + url;
		}
		return new Promise((resolve, reject) => {
			axios({
				method: 'get',
				url: url,
				params: params,
				...config
			}).then((response) => {
				resolve(response.data);
			}).catch((error) => {
				reject(error);
			})
		})
	},

	/** post 请求
	 * @param  {接口地址} url
	 * @param  {请求参数} data
	 * @param  {参数} config
	 */
	post: function (url, data = {}, config = {}) {

		if (url.indexOf("http") == -1) {
			url = sysConfig.REMOTE_URL + url;
		}
		let token = tool.data.get("TOKEN");
		if (token) {
			data.userToken = token;
		}
		data.tenant = sysConfig.TENANT;

		return new Promise((resolve, reject) => {
			axios({
				method: 'post',
				url: url,
				data: data,
				...config
			}).then((response) => {
				resolve(response.data);
			}).catch((error) => {
				reject(error);
			})
		})
	},

	/** form 请求
	 * @param  {接口地址} url
	 * @param  {请求参数} data
	 * @param  {参数} config
	 */
	form: function (url, data = {}, config = {}) {

		if (url.indexOf("http") == -1) {
			url = sysConfig.REMOTE_URL + url;
		}
		let token = tool.data.get("TOKEN");
		if (token) {
			data.userToken = token;
		}
		data.tenant = sysConfig.TENANT;


		return new Promise((resolve, reject) => {
			axios({
				method: 'post',
				url: url,
				headers: {
					"Content-type": "application/x-www-form-urlencoded"
				},
				data: qs.stringify(data),
				...config
			}).then((response) => {
				resolve(response.data);
			}).catch((error) => {
				reject(error);
			})
		})
	},

	/** put 请求
	 * @param  {接口地址} url
	 * @param  {请求参数} data
	 * @param  {参数} config
	 */
	put: function (url, data = {}, config = {}) {
		if (url.indexOf("http") == -1) {
			url = sysConfig.REMOTE_URL + url;
		}
		return new Promise((resolve, reject) => {
			axios({
				method: 'put',
				url: url,
				data: data,
				...config
			}).then((response) => {
				resolve(response.data);
			}).catch((error) => {
				reject(error);
			})
		})
	},

	/** patch 请求
	 * @param  {接口地址} url
	 * @param  {请求参数} data
	 * @param  {参数} config
	 */
	patch: function (url, data = {}, config = {}) {
		if (url.indexOf("http") == -1) {
			url = sysConfig.REMOTE_URL + url;
		}
		return new Promise((resolve, reject) => {
			axios({
				method: 'patch',
				url: url,
				data: data,
				...config
			}).then((response) => {
				resolve(response.data);
			}).catch((error) => {
				reject(error);
			})
		})
	},

	/** delete 请求
	 * @param  {接口地址} url
	 * @param  {请求参数} data
	 * @param  {参数} config
	 */
	delete: function (url, data = {}, config = {}) {
		if (url.indexOf("http") == -1) {
			url = sysConfig.REMOTE_URL + url;
		}
		return new Promise((resolve, reject) => {
			axios({
				method: 'delete',
				url: url,
				data: data,
				...config
			}).then((response) => {
				resolve(response.data);
			}).catch((error) => {
				reject(error);
			})
		})
	}
}

export default http;