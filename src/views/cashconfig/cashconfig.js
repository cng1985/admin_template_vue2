export default [
    {
        name: "cashconfig_home",
        path: "/cashconfig/index",
        component: () => import(`@/views/cashconfig/index`),
        meta: {
            title: "提现配置管理",
            icon: "el-icon-platform-eleme"
        }
    },
    {
        name: "cashconfig_add",
        path: "/cashconfig/add",
        component: () => import(`@/views/cashconfig/add`),
        meta: {
            title: "添加提现配置",
            icon: "el-icon-platform-eleme"
        }
    },
    {
        name: "cashconfig_update",
        path: "/cashconfig/update",
        component: () => import(`@/views/cashconfig/update`),
        meta: {
            title: "更新提现配置",
            icon: "el-icon-platform-eleme"
        }
    },
    {
        name: "cashconfig_view",
        path: "/cashconfig/view",
        component: () => import(`@/views/cashconfig/view`),
        meta: {
            title: "提现配置详情",
            icon: "el-icon-platform-eleme"
        }
    },
]