export default [
    {
        name: "device_home",
        path: "/device/index",
        component: () => import(`@/views/device/index`),
        meta: {
            title: "打印机管理",
            icon: "el-icon-platform-eleme"
        }
    },
    {
        name: "device_add",
        path: "/device/add",
        component: () => import(`@/views/device/add`),
        meta: {
            title: "添加打印机",
            icon: "el-icon-platform-eleme"
        }
    },
    {
        name: "device_update",
        path: "/device/update",
        component: () => import(`@/views/device/update`),
        meta: {
            title: "更新打印机",
            icon: "el-icon-platform-eleme"
        }
    },
    {
        name: "device_layout",
        path: "/device/view_layout",
        component: () => import(`@/views/device/view_layout`),
        meta: {
            title: "打印机详情",
            icon: "el-icon-platform-eleme"
        },
        children:[
            {
                name: "device_view",
                path: "/device/view",
                component: () => import(`@/views/device/view`),
                meta: {
                    title: "打印机详情",
                    icon: "el-icon-platform-eleme"
                }
            }
        ]

    }
]