export default [{
        name: "home",
        path: "/dishcouponrule/index",
        component: () => import(`@/views/dishcouponrule/index`),
        meta: {
            title: "优惠券管理",
            icon: "el-icon-platform-eleme"
        }
    },
    {
        name: "home",
        path: "/dishcouponrule/add",
        component: () => import(`@/views/dishcouponrule/add`),
        meta: {
            title: "添加优惠券",
            icon: "el-icon-platform-eleme"
        }
    },
    {
        name: "home",
        path: "/dishcouponrule/update",
        component: () => import(`@/views/dishcouponrule/update`),
        meta: {
            title: "更新优惠券",
            icon: "el-icon-platform-eleme"
        }
    },
    {
        name: "home",
        path: "/dishcouponrule/view",
        component: () => import(`@/views/dishcouponrule/view`),
        meta: {
            title: "优惠券详情",
            icon: "el-icon-platform-eleme"
        }
    },
]