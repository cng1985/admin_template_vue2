export default [
    {
        name: "dynamicpageitem_home",
        path: "/dynamicpageitem/index",
        component: () => import(`@/views/dynamicpageitem/index`),
        meta: {
            title: "动态页面管理",
            icon: "el-icon-platform-eleme"
        }
    },
    {
        name: "dynamicpageitem_add",
        path: "/dynamicpageitem/add",
        component: () => import(`@/views/dynamicpageitem/add`),
        meta: {
            title: "添加动态页面",
            icon: "el-icon-platform-eleme"
        }
    },
    {
        name: "dynamicpageitem_update",
        path: "/dynamicpageitem/update",
        component: () => import(`@/views/dynamicpageitem/update`),
        meta: {
            title: "更新动态页面",
            icon: "el-icon-platform-eleme"
        }
    },
    {
        name: "dynamicpageitem_view",
        path: "/dynamicpageitem/view",
        component: () => import(`@/views/dynamicpageitem/view`),
        meta: {
            title: "动态页面详情",
            icon: "el-icon-platform-eleme"
        }
    },
]