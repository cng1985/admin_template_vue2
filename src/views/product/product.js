export default [
    {
        name: "product_home",
        path: "/product/index",
        component: () => import(`@/views/product/index`),
        meta: {
            title: "商品管理",
            icon: "el-icon-platform-eleme"
        }
    },
    {
        name: "product_add",
        path: "/product/add",
        component: () => import(`@/views/product/add`),
        meta: {
            title: "添加商品",
            icon: "el-icon-platform-eleme"
        }
    },
    {
        name: "product_update",
        path: "/product/update",
        component: () => import(`@/views/product/update`),
        meta: {
            title: "更新商品",
            icon: "el-icon-platform-eleme"
        }
    },
    {
        name: "product_layout",
        path: "/product/view_layout",
        component: () => import(`@/views/product/view_layout`),
        meta: {
            title: "商品详情",
            icon: "el-icon-platform-eleme"
        },
        children:[
            {
                name: "product_view",
                path: "/product/view",
                component: () => import(`@/views/product/view`),
                meta: {
                    title: "商品详情",
                    icon: "el-icon-platform-eleme"
                }
            }
        ]

    }
]