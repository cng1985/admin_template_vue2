export default [
    {
        name: "productcatalog_home",
        path: "/productcatalog/index",
        component: () => import(`@/views/productcatalog/index`),
        meta: {
            title: "商品分类管理",
            icon: "el-icon-platform-eleme"
        }
    }
]