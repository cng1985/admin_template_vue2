export default [
    {
        name: "statistic_home",
        path: "/statistic/index",
        component: () => import(`@/views/statistic/index`),
        meta: {
            title: "管理",
            icon: "el-icon-platform-eleme"
        }
    },
    {
        name: "statistic_add",
        path: "/statistic/add",
        component: () => import(`@/views/statistic/add`),
        meta: {
            title: "添加",
            icon: "el-icon-platform-eleme"
        }
    },
    {
        name: "statistic_update",
        path: "/statistic/update",
        component: () => import(`@/views/statistic/update`),
        meta: {
            title: "更新",
            icon: "el-icon-platform-eleme"
        }
    },
    {
        name: "statistic_view",
        path: "/statistic/view",
        component: () => import(`@/views/statistic/view`),
        meta: {
            title: "详情",
            icon: "el-icon-platform-eleme"
        }
    },
]