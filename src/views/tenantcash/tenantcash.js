export default [
    {
        name: "tenantcash_home",
        path: "/tenantcash/index",
        component: () => import(`@/views/tenantcash/index`),
        meta: {
            title: "提现记录管理",
            icon: "el-icon-platform-eleme"
        }
    },
    {
        name: "tenantcash_add",
        path: "/tenantcash/add",
        component: () => import(`@/views/tenantcash/add`),
        meta: {
            title: "添加提现记录",
            icon: "el-icon-platform-eleme"
        }
    },
    {
        name: "tenantcash_update",
        path: "/tenantcash/update",
        component: () => import(`@/views/tenantcash/update`),
        meta: {
            title: "更新提现记录",
            icon: "el-icon-platform-eleme"
        }
    },
    {
        name: "tenantcash_layout",
        path: "/tenantcash/view_layout",
        component: () => import(`@/views/tenantcash/view_layout`),
        meta: {
            title: "提现记录详情",
            icon: "el-icon-platform-eleme"
        },
        children:[
            {
                name: "tenantcash_view",
                path: "/tenantcash/view",
                component: () => import(`@/views/tenantcash/view`),
                meta: {
                    title: "提现记录详情",
                    icon: "el-icon-platform-eleme"
                }
            }
        ]

    }
]