export default [
    {
        name: "tenantuserrole_home",
        path: "/tenantuserrole/index",
        component: () => import(`@/views/tenantuserrole/index`),
        meta: {
            title: "角色管理",
            icon: "el-icon-platform-eleme"
        }
    },
    {
        name: "tenantuserrole_add",
        path: "/tenantuserrole/add",
        component: () => import(`@/views/tenantuserrole/add`),
        meta: {
            title: "添加角色",
            icon: "el-icon-platform-eleme"
        }
    },
    {
        name: "tenantuserrole_update",
        path: "/tenantuserrole/update",
        component: () => import(`@/views/tenantuserrole/update`),
        meta: {
            title: "更新角色",
            icon: "el-icon-platform-eleme"
        }
    },
    {
        name: "tenantuserrole_view",
        path: "/tenantuserrole/view",
        component: () => import(`@/views/tenantuserrole/view`),
        meta: {
            title: "角色详情",
            icon: "el-icon-platform-eleme"
        }
    },
]